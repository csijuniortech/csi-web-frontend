### What is this repository for? ###

* CSI website front end 

### Languages and libraries used? ###

* HTML5
* CSS3
* SASS
* JQuery
* BOURBON (SASS mixin library)
* Font-awesome (for icons)

### Contribution guidelines ###

* All the css files will go in css directory and JQuery files in js directory
* Standard mixins (Which could be applied on all the pages) should be added in _mixins.scss file in mixins directory, else just create a new mixin
* Always import _navbar.scss and _bourbon.scss while working on scss file for a new page
    @_import 'navbar' 
    @_import 'bourbon/bourbon'
(Remove _ between @ and import)